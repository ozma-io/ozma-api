// Legacy entry point.

import FunDBClient from "./client";

export * from "./client";
export * from "./embedded/utils";
export * as Embedded from "./embedded/types";
export default FunDBClient;
